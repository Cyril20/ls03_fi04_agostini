package view;


import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Gui extends JFrame {

	private JPanel contentPane;
	private JTextField txtfield;
	private Color standardColor;
	private JLabel mainText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui frame = new Gui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	/**
	 * Create the frame.
	 */
	public Gui() {
		setTitle("Form_aendern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		standardColor = contentPane.getBackground();
		
		mainText = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		mainText.setFont(new Font("Tahoma", Font.BOLD, 12));
		mainText.setForeground(Color.DARK_GRAY);
		mainText.setHorizontalAlignment(SwingConstants.CENTER);
		mainText.setBounds(0, 0, 384, 85);
		contentPane.add(mainText);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setForeground(Color.DARK_GRAY);
		lblAufgabe1.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe1.setBounds(10, 96, 297, 14);
		contentPane.add(lblAufgabe1);
		
		JPanel bgColorPane = new JPanel();
		bgColorPane.setBounds(10, 115, 370, 58);
		bgColorPane.setBackground(new Color(0,0,0,0));
		contentPane.add(bgColorPane);
		bgColorPane.setLayout(new GridLayout(2, 2, 10, 5));
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBgColor(Color.red);
			}
		});
		bgColorPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBgColor(Color.green);
			}

		});
		bgColorPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBgColor(Color.blue);
			}
		});
		bgColorPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBgColor(Color.yellow);
			}
		});
		bgColorPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setBgColor(standardColor);
			}
		});
		bgColorPane.add(btnStandardfarbe);
		
		JButton btnFarbeWaehlen = new JButton("Farbe w\u00E4hlen");
		btnFarbeWaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Color newColor = JColorChooser.showDialog(null, "W�hle Farbe", contentPane.getBackground());
				setBgColor(newColor);
			}
		});
		bgColorPane.add(btnFarbeWaehlen);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setForeground(Color.DARK_GRAY);
		lblAufgabe2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe2.setBounds(10, 184, 297, 14);
		contentPane.add(lblAufgabe2);
		
		JPanel fontPane = new JPanel();
		fontPane.setBounds(10, 203, 370, 26);
		fontPane.setBackground(new Color(0,0,0,0));
		contentPane.add(fontPane);
		fontPane.setLayout(new GridLayout(1, 2, 10, 0));
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtFont("Arial");
			}
		});
		fontPane.add(btnArial);
		
		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.setMargin(new Insets(0,0,0,0));
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtFont("Comic Sans MS");
			}
		});
		btnComicSansMs.setToolTipText("Comic Sans MS");
		fontPane.add(btnComicSansMs);
		
		JButton btnCourrierNew = new JButton("Courrier New");
		btnCourrierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtFont("Courier New");
			}
		});
		fontPane.add(btnCourrierNew);
		
		txtfield = new JTextField();
		txtfield.setText("Hier bitte Text eingeben");
		txtfield.setBounds(10, 237, 374, 20);
		contentPane.add(txtfield);
		txtfield.setColumns(10);
		
		JPanel labelPane = new JPanel();
		labelPane.setBounds(10, 263, 370, 26);
		labelPane.setBackground(new Color(0,0,0,0));
		contentPane.add(labelPane);
		labelPane.setLayout(new GridLayout(1, 2, 5, 0));
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainText.setText(txtfield.getText());
			}
		});
		labelPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainText.setText("");
			}
		});
		labelPane.add(btnTextImLabel);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setForeground(Color.DARK_GRAY);
		lblAufgabe3.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe3.setBounds(10, 300, 297, 14);
		contentPane.add(lblAufgabe3);
		
		JPanel txtColorPane = new JPanel();
		txtColorPane.setBounds(10, 318, 370, 26);
		txtColorPane.setBackground(new Color(0,0,0,0));
		contentPane.add(txtColorPane);
		txtColorPane.setLayout(new GridLayout(1, 2, 10, 0));
		
		JButton btnTxtRot = new JButton("Rot");
		btnTxtRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtColor(Color.red);
			}
		});
		txtColorPane.add(btnTxtRot);
		
		JButton btnTxtBlau = new JButton("Blau");
		btnTxtBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtColor(Color.blue);
			}
		});
		btnTxtBlau.setToolTipText("Comic Sans MS");
		txtColorPane.add(btnTxtBlau);
		
		JButton btnTxtSchwarz = new JButton("Schwarz");
		btnTxtSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTxtColor(Color.black);
			}
		});
		txtColorPane.add(btnTxtSchwarz);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setForeground(Color.DARK_GRAY);
		lblAufgabe4.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe4.setBounds(10, 355, 297, 14);
		contentPane.add(lblAufgabe4);
		
		JPanel fontSizePane = new JPanel();
		fontSizePane.setBounds(10, 375, 370, 26);
		fontSizePane.setBackground(new Color(0,0,0,0));
		contentPane.add(fontSizePane);
		fontSizePane.setLayout(new GridLayout(1, 2, 5, 0));
		
		JButton btnPlus = new JButton("+");
		btnPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font txtFont = mainText.getFont();
				mainText.setFont(txtFont.deriveFont(txtFont.getSize()+1f));
			}
		});
		fontSizePane.add(btnPlus);
		
		JButton btnMinus = new JButton("-");
		btnMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font txtFont = mainText.getFont();
				mainText.setFont(txtFont.deriveFont(txtFont.getSize()-1f));
			}
		});
		fontSizePane.add(btnMinus);
		
		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setForeground(Color.DARK_GRAY);
		lblAufgabe5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe5.setBounds(10, 412, 297, 14);
		contentPane.add(lblAufgabe5);
		
		JPanel txtAlignPane = new JPanel();
		txtAlignPane.setBounds(10, 431, 370, 26);
		txtAlignPane.setBackground(new Color(0,0,0,0));
		contentPane.add(txtAlignPane);
		txtAlignPane.setLayout(new GridLayout(1, 2, 10, 0));
		
		JButton btnLeftAlign = new JButton("linksb\u00FCndig");
		btnLeftAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainText.setHorizontalAlignment(JLabel.LEFT);
			}
		});
		txtAlignPane.add(btnLeftAlign);
		
		JButton btnCenterAlign = new JButton("zentriert");
		btnCenterAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainText.setHorizontalAlignment(JLabel.CENTER);
			}
		});
		btnCenterAlign.setToolTipText("Comic Sans MS");
		txtAlignPane.add(btnCenterAlign);
		
		JButton btnRightAlign = new JButton("rechtsb\u00FCndig");
		btnRightAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mainText.setHorizontalAlignment(JLabel.RIGHT);
			}
		});
		txtAlignPane.add(btnRightAlign);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setForeground(Color.DARK_GRAY);
		lblAufgabe6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAufgabe6.setBounds(10, 468, 297, 14);
		contentPane.add(lblAufgabe6);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(10, 486, 370, 68);
		contentPane.add(btnExit);
	}
	
	public void setBgColor(Color color) {
		this.contentPane.setBackground(color);
		
	}
	public void setTxtFont(String font) {
		this.mainText.setFont(new Font(font, Font.PLAIN, this.mainText.getFont().getSize()));
	}
	public void setTxtColor(Color color) {
		this.mainText.setForeground(color);
		
	}
}
