package view.menue;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import model.User;

import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.Box;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.awt.event.ActionEvent;
import java.awt.Cursor;

//TODO create a usermanagement
public class CreateUserWindow extends JFrame {
	private JPanel contentPane;
	private JTextField inputName;
	private JTextField inputLastName;
	private JTextField inputBirth;
	private JTextField inputStreet;
	private JTextField inputHouseNumber;
	private JTextField inputPostalCode;
	private JTextField inputCity;
	private JTextField inputLoginName;
	private JPasswordField inputPassword;
	private JTextField inputSalary;
	private JTextField inputMarital;
	private JTextField inputNote;
	private JPanel form;
	private JLabel labelError;
	private AlienDefenceController alienDefenceController;

	public CreateUserWindow(AlienDefenceController alienDefenceController) {
		this.alienDefenceController = alienDefenceController;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(300, 100, 600, 800);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

		JLabel labelTitle = new JLabel("KONTO ERSTELLEN");
		labelTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		labelTitle.setHorizontalAlignment(SwingConstants.CENTER);
		labelTitle.setForeground(new Color(124, 252, 0));
		labelTitle.setFont(new Font("Yu Gothic UI", Font.BOLD, 50));
		contentPane.add(labelTitle);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		contentPane.add(verticalStrut);

		form = new JPanel();
		form.setBackground(Color.BLACK);
		contentPane.add(form);
		form.setLayout(new GridLayout(0, 2, 0, 20));
		form.setMaximumSize(new Dimension(400, 550));

		JLabel lblVorname = new JLabel("Vorname:");
	
		form.add(lblVorname);
		lblVorname.setForeground(Color.ORANGE);

		inputName = new JTextField();
		form.add(inputName);


		JLabel lblNachname = new JLabel("Nachname:");
		lblNachname.setForeground(Color.ORANGE);
		form.add(lblNachname);

		inputLastName = new JTextField();
	
		form.add(inputLastName);

		JLabel lblBirth = new JLabel("Geburtsdatum:");
		lblBirth.setForeground(Color.ORANGE);
		form.add(lblBirth);

		inputBirth = new JTextField();
		inputBirth.setToolTipText("z.B 25.07.1997");
		
		form.add(inputBirth);

		JLabel lblStreet = new JLabel("Stra\u00DFe:");
	
		lblStreet.setForeground(Color.ORANGE);
		form.add(lblStreet);

		inputStreet = new JTextField();
		inputStreet.setToolTipText("z.B: Teststra\u00DFe");
	
		form.add(inputStreet);

		JLabel lblHouseNumber = new JLabel("Hausnummer:");
		form.add(lblHouseNumber);
	
		lblHouseNumber.setForeground(Color.ORANGE);

		inputHouseNumber = new JTextField();
		form.add(inputHouseNumber);
		inputHouseNumber.setToolTipText("z.B: 123");
	
		
		JLabel lblPostalCode = new JLabel("PLZ:");
	
		lblPostalCode.setForeground(Color.ORANGE);
		form.add(lblPostalCode);
		
		inputPostalCode = new JTextField();
		inputPostalCode.setToolTipText("z.B: 12547");
	
		form.add(inputPostalCode);
		
		JLabel lblCity = new JLabel("Stadt:");
		lblCity.setForeground(Color.ORANGE);
		form.add(lblCity);
		
		inputCity = new JTextField();
		inputCity.setToolTipText("z.B: Berlin");

		form.add(inputCity);
		
		JLabel lblLoginName = new JLabel("Benutzername:");
		lblLoginName.setForeground(Color.ORANGE);
		form.add(lblLoginName);
		
		inputLoginName = new JTextField();
		inputLoginName.setToolTipText("z.B: name123");
		form.add(inputLoginName);
		
		JLabel lblPassword = new JLabel("Passwort:");
		lblPassword.setForeground(Color.ORANGE);
		form.add(lblPassword);
		
		inputPassword = new JPasswordField();
		form.add(inputPassword);
		
		JLabel lblSalary = new JLabel("Gehaltsvorstellungen:");
		lblSalary.setForeground(Color.ORANGE);
		form.add(lblSalary);
		
		inputSalary = new JTextField();
		inputSalary.setToolTipText("z.B: 36000 (pro jahr)");
		form.add(inputSalary);
		
		JLabel lblMarital = new JLabel("Familienstand:");
		lblMarital.setForeground(Color.ORANGE);
		form.add(lblMarital);
		
		inputMarital = new JTextField();
		
		inputMarital.setToolTipText("z.B: ledig");
		form.add(inputMarital);
		
		JLabel lblNote = new JLabel("Abschlussnote:");
		lblNote.setForeground(Color.ORANGE);
		form.add(lblNote);
		
		inputNote = new JTextField();
		inputNote.setToolTipText("z.B: 2.4");
		form.add(inputNote);
		
		JButton btnSubmit = new JButton("Erstellen");
		btnSubmit.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnSubmit.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSubmit.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSubmitClicked();
			}
		});
		
		Component verticalStrut_1 = Box.createVerticalStrut(20);
		contentPane.add(verticalStrut_1);
		contentPane.add(btnSubmit);
		
		Component verticalStrut_1_1 = Box.createVerticalStrut(20);
		contentPane.add(verticalStrut_1_1);
		
		labelError = new JLabel("");
		labelError.setHorizontalAlignment(SwingConstants.CENTER);
		labelError.setForeground(Color.RED);
		labelError.setFont(new Font("Yu Gothic UI", Font.BOLD, 15));
		labelError.setAlignmentX(0.5f);
		contentPane.add(labelError);

		this.setVisible(true);
	}
	
	private void btnSubmitClicked() {
		int emptyFields = 0;
		Component[] children = form.getComponents();
	    for (Component c : children) {
	        if (c instanceof JTextField) {
	            JTextField textField = (JTextField)c;
	            if(textField.getText().length() == 0) {
	            	emptyFields++;
	            }
	        }
	        else if (c instanceof JPasswordField) {
	            JPasswordField textField = (JPasswordField)c;
	            if(textField.getPassword().length == 0) {
	            	emptyFields++;
	            }
	        }
	    }
	    if(emptyFields > 0) {
	    	  labelError.setText(emptyFields + " Felder sind leer.");
	    	  return;
	    }
	    labelError.setText("");
	    
	    String first_name = inputName.getText();
	    String sur_name = inputLastName.getText();
	    
	    LocalDate birthday;
	    try {
	    	 birthday = LocalDate.parse(inputBirth.getText(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
		} catch (DateTimeParseException e) {
			labelError.setText("Geburtsdatum ist falsch (Format: dd.mm.yyyy)");
			return;
		}
	   
	    String street = inputStreet.getText();
	    String house_number = inputHouseNumber.getText();
	    String postal_code = inputPostalCode.getText();
	    String city = inputCity.getText();
	    String loginname = inputLoginName.getText();
	    String password = String.valueOf(inputPassword.getPassword());
	    
	    int salary_expectations;
	    try {
			salary_expectations = Integer.parseInt(inputSalary.getText());
		} catch (NumberFormatException e) {
			labelError.setText("Der Wert der Gehaltsvorstellungen ist falsch (z.B: 74213)");
			return;
		}
	    
	    String marital_status = inputMarital.getText();
	    
	    double final_grade;
		try {
			final_grade = Double.parseDouble(inputNote.getText());
		} catch (NumberFormatException e) {
			labelError.setText("Der Wert der Abschlussnote ist falsch (z.B: 3.4)");
			return;
		}
		
	    
	    User user = new User(0, first_name, sur_name, birthday, street, house_number, postal_code, city, loginname, password, salary_expectations, marital_status, final_grade);
	    
	    boolean created = alienDefenceController.getUserController().createUser(user);
	    if(!created) {
	    	labelError.setText("Der Benutzername existiert schon oder ein Fehler ist aufgetreten");
			return;
	    }
	    JOptionPane.showMessageDialog(null, "Ihr Konto wurde erfolgreich erstellt", "Konto Erstellen",
				JOptionPane.INFORMATION_MESSAGE);
	    this.dispose();
	    
	}
	



}
