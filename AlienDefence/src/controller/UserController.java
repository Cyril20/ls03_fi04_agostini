package controller;


import model.User;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * 
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	
	/**
	 * erstellt einen Benutzer
	 * @param user objekt benutzer
	 * @return true, wenn der benutzer erfolgreich erstellt wurde
	 */
	public boolean createUser(User user) {
		User exists = this.userPersistance.readUser(user.getLoginname());
		if(exists == null) {
			int lastKey = this.userPersistance.createUser(user);
			if(lastKey != -1) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		User user = this.userPersistance.readUser(username);
		if(user != null && checkPassword(user.getPassword(), passwort)) {
			return user;
		}
		return null;
	}
	
	/**
	 * �ndert die Daten des Benutzers
	 * @param user objekt benutzer
	 */
	public void changeUser(User user) {
		this.userPersistance.updateUser(user);
	}
	
	/**
	 * entfernt den Benutzer aus der Datenbank
	 * @param user objekt benutzer
	 */
	public void deleteUser(User user) {
		this.userPersistance.deleteUser(user.getP_user_id());
	}
	
	/**
	 * pr�ft, ob zwei Passw�rter identisch sind
	 * @param userPasswort passwort des Benutzerobjekts
	 * @param passwort das richtige Passwort
	 * @return false, true wenn die zwei Passw�rter identisch sind
	 */
	private boolean checkPassword(String userPasswort, String passwort) {
		
		if(userPasswort.equals(passwort)) {
			return true;
		}
		return false;
	}
}
