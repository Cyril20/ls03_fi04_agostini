package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import com.sun.xml.internal.ws.api.policy.AlternativeSelector;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private JPanel pnlButtons;


	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 * @param fromSpielenBtn
	 *            Wurde die Klasse implementiert, als das Button "Spielen" geklickt
	 *            wurde?
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow) {
		setBackground(new Color(0, 0, 0));
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblLevelauswahl.setForeground(new Color(124, 252, 0));
		lblLevelauswahl.setBackground(new Color(0, 0, 0));
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 30));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setBackground(new Color(0, 0, 0));
		spnLevels.getViewport().setBackground(Color.black);

		Component verticalStrut = Box.createVerticalStrut(15);
		add(verticalStrut);
		add(spnLevels);

		tblLevels = new JTable();
		tblLevels.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		tblLevels.setSelectionBackground(Color.ORANGE);
		tblLevels.setForeground(Color.WHITE);
		tblLevels.setBackground(new Color(50, 50, 50));
		tblLevels.setRowHeight(30);
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);
		JTableHeader header = tblLevels.getTableHeader();
		header.setBackground(new Color(40, 40, 40));
		header.setForeground(Color.ORANGE);
		header.setFont(header.getFont().deriveFont(Font.BOLD));
		this.updateTableData();

		pnlButtons = new JPanel();
		pnlButtons.setBackground(new Color(0, 0, 0));

		add(pnlButtons);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnNewLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnUpdateLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnDeleteLevel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

	}

	public void setBtnSpielen(AlienDefenceController alienDefenceController, User user) {
		this.pnlButtons.removeAll();
		
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setFont(new Font("Tahoma", Font.PLAIN, 22));
		btnSpielen.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSpielen_Clicked(alienDefenceController, user);
			}
		});
		this.pnlButtons.add(btnSpielen);

	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();

		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
		

	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
		int selectedRow = this.tblLevels.getSelectedRow();
		if(selectedRow == -1) {
			JOptionPane.showMessageDialog(null, "Bitte w�hlen Sie ein Level", "Fehler", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Object cellValue = this.tblLevels.getModel().getValueAt(selectedRow, 0);
		int level_id = Integer.parseInt((String) cellValue);
		Level level = alienDefenceController.getLevelController().readLevel(level_id);
		
		if(level == null) {
			System.out.println("level #"+level_id+" wurde nicht gefunden");
			return;
		}
		
		Thread t = new Thread("GameThread") {
			@Override
			public void run() {
				GameController gameController = alienDefenceController.startGame(level, user);
				new GameGUI(gameController).start();
			}
		};
		t.start();
		this.leveldesignWindow.dispose();
	}
}
