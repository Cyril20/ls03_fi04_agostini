package view.menue;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import controller.GameController;
import model.Level;
import model.persistanceDB.PersistanceDB;
import model.User;
import view.game.GameGUI;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.Box;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;

public class MainMenue extends JFrame {

	private static final long serialVersionUID = 1L;
	private AlienDefenceController alienDefenceController;
	private JPanel contentPane;
	private JTextField loginTextField;
	private JPasswordField passwordTextField;
	private List<Level> arrLevel;

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MainMenue(AlienDefenceController alienDefenceController) {

		this.alienDefenceController = alienDefenceController;

		// Allgemeine JFrame-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		// setUndecorated(true);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		// �berschrift
		JLabel lblHeadline = new JLabel("ALIEN DEFENCE");
		lblHeadline.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblHeadline.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadline.setForeground(new Color(124, 252, 0));
		lblHeadline.setFont(new Font("Yu Gothic UI", Font.BOLD, 50));
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("./pictures/logo.png").getImage().getScaledInstance(140, 140, Image.SCALE_DEFAULT));

		// Logo
		JPanel pnlLogo = new JPanel();
		pnlLogo.setBackground(Color.BLACK);
		pnlLogo.setMaximumSize(new Dimension(150, 150));

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(imageIcon);
		pnlLogo.add(lblLogo);
		contentPane.add(lblHeadline);
		contentPane.add(pnlLogo);

		Component verticalStrut_1 = Box.createVerticalStrut(30);
		contentPane.add(verticalStrut_1);

		JPanel pnlLogin = new JPanel();
		pnlLogin.setBackground(Color.BLACK);
		contentPane.add(pnlLogin);
		pnlLogin.setLayout(new GridLayout(0, 1, 0, 0));
		pnlLogin.setMaximumSize(new Dimension(600, 150));

		// Nutzername
		JLabel lblLogin = new JLabel("Login:");
		pnlLogin.add(lblLogin);
		lblLogin.setForeground(Color.orange);

		loginTextField = new JTextField();
		pnlLogin.add(loginTextField);
		loginTextField.setColumns(10);

		// Passwort
		JLabel lblPassword = new JLabel("Passwort:");
		pnlLogin.add(lblPassword);
		lblPassword.setForeground(Color.orange);

		passwordTextField = new JPasswordField();
		pnlLogin.add(passwordTextField);

		Component verticalStrut = Box.createVerticalStrut(30);
		contentPane.add(verticalStrut);
		
		
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		pnlButtons.setLayout(new GridLayout(0, 1, 0, 20));
		pnlButtons.setMaximumSize(new Dimension(600, 300));

		// Levelnamen auslesen
		arrLevel = this.alienDefenceController.getLevelController().readAllLevels();
		String[] arrLevelNames = getLevelNames(arrLevel);

		// Spiel starten
		// Die Textfelder werden ausgewertet und das Passwort validiert, dann wird das
		// Spiel gestartet
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		
		btnSpielen.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// User aus Datenbank holen
				User user = alienDefenceController.getUserController().readUser(loginTextField.getText(), new String(passwordTextField.getPassword()));

				if (user != null) {

					Thread t = new Thread("GameThread") {
						@Override
						public void run() {

							GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
							new GameGUI(gameController).start();
						}
					};
					t.start();
				} else {
					// Fehlermeldung - Zugangsdaten fehlerhaft
					JOptionPane.showMessageDialog(null, "Zugangsdaten nicht korrekt", "Fehler",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		pnlButtons.add(btnSpielen);

		JButton btnTesten = new JButton("Testen");
		btnTesten.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnTesten.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnTesten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				User user = new User(1, "test", "pass");
			
				LeveldesignWindow leveldesignWindow = new LeveldesignWindow(alienDefenceController.getLevelController(),
				alienDefenceController.getTargetController());
				
				leveldesignWindow.getCardChooseLevel().setBtnSpielen(alienDefenceController, user);

			}
		});
		pnlButtons.add(btnTesten);

		JButton btnRegister = new JButton("Konto erstellen");
		btnRegister.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnRegister.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CreateUserWindow(alienDefenceController);
		
			}
		});
		pnlButtons.add(btnRegister);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnHighscore.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] arrLevelNames = getLevelNames(arrLevel);
				String level = (String) JOptionPane.showInputDialog(null, "W�hlen Sie ein Level", "Levelauswahl",
						JOptionPane.PLAIN_MESSAGE, null, arrLevelNames, arrLevelNames[0]);
				
				if(level != null) {
					int level_id = -1;
					for (int i = 0; i < arrLevelNames.length; i++) {
						if(arrLevelNames[i].equals(level)) {
							level_id = i;
							break;
						}
					}
					if(level_id != -1) {
						new Highscore(alienDefenceController.getAttemptController(), level_id);
					}
				}
			}
		});

		pnlButtons.add(btnHighscore);
		
		

		JButton btnLeveleditor = new JButton("Leveleditor");
		btnLeveleditor.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnLeveleditor.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnLeveleditor.setBackground(Color.ORANGE);
		btnLeveleditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LeveldesignWindow(alienDefenceController.getLevelController(),
						alienDefenceController.getTargetController());
			}
		});
		pnlButtons.add(btnLeveleditor);

		JButton btnBeenden = new JButton("Beenden");
		btnBeenden.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnBeenden.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnBeenden.setBackground(new Color(204, 51, 51));
		btnBeenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		pnlButtons.add(btnBeenden);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		


		contentPane.add(pnlButtons);

	}

	private String[] getLevelNames(List<Level> arrLevel) {
		String[] arrLevelNames = new String[arrLevel.size()];

		for (int i = 0; i < arrLevel.size(); i++) {
			arrLevelNames[i] = arrLevel.get(i).getName(); // Array aus Arraylist erstellt
		}

		return arrLevelNames;
	}

	
}
